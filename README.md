My dotfiles managed with [dotdrop](https://github.com/deadc0de6/dotdrop.git).

# Carbon X11
To be added in acpid handler.sh in order to have backlight and volume control
with hotkeys:

```txt
case "$1" in
    button/mute)
        amixer set Master toggle
        ;;
    button/volumedown)
        amixer set Master 5-
        ;;
    button/volumeup)
        amixer set Master 5+
        ;;
    video/brightnessup)
        /home/random/bin/backlight.sh -i -s 100
        ;;
    video/brightnessdown)
        /home/random/bin/backlight.sh -d -s 100
        ;;
# ...
```
