#!/bin/sh
BG_IMAGE=/tmp/lock-bg.png

scrot -z ${BG_IMAGE}
convert ${BG_IMAGE} -scale 10% ${BG_IMAGE}
convert ${BG_IMAGE} -scale 1000% ${BG_IMAGE}
i3lock --image=${BG_IMAGE} --nofork --ignore-empty-password

rm ${BG_IMAGE}
