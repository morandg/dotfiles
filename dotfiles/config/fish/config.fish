#if [ "$DISPLAY" = "" ]
#  exec startx 
#end

set -x PATH $PATH ~/bin ~/stow/sysroot/bin
set -x MANPATH "/usr/share/man/:/opt/my-sysroot/share/man/"

function m
  make -j(cat /proc/cpuinfo | grep processor | wc -l) $argv
end

