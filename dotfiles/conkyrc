--[[
Conky, a system monitor, based on torsmo

Any original torsmo code is licensed under the BSD license

All code written since the fork of torsmo is licensed under the GPL

Please see COPYING for details

Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
Copyright (c) 2005-2019 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]
conky.config = {
    alignment = 'top_right',
    background = true,
    border_width = 0,
    cpu_avg_samples = 2,
    default_color = 'gray',
    default_outline_color = 'black',
    default_shade_color = 'white',
    double_buffer = true,
    extra_newline = false,
    font = 'DejaVu Sans Mono:size=12',
    gap_x = 10,
    gap_y = 20,
    minimum_width = 300,
    maximum_width = 300,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'override',
    own_window_transparent = false,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    update_interval = 3.0,
    uppercase = false,
    use_spacer = 'none',
    use_xft = true,
    border_inner_margin = 0,
    border_outer_margin = 10,
    own_window_argb_visual = true,
    own_window_argb_value = 180,
    own_window_colour = '#000000',
    draw_borders = false,
    draw_graph_borders = false,
    draw_shades = false,
    draw_outline = false,
    default_color = '#000000',
    color1 = '#EEEEEE',
    color2 = '#DCD27E',
    color3 = '#FBFFFE',
}

conky.text = [[
#------------+
# INFO
#------------+
${voffset 10}${alignc}${color1}${font :size=16:bold}${time %D}${font}
${color1}${alignc}${font :size=12:italic}${time %H:%M:%S}${font}
${voffset 10}${color1}Kernel${alignr}${color3} $kernel
${color1}Uptime${alignr}${color3} $uptime
${color3}${battery_status}${alignr}${battery_time}
${color3}${battery_bar 10,100}${alignr}${battery_percent}%
#------------+
#CPU
#------------+
${voffset 20}${color1}${font :size=14:bold}CPU ${font}${hr 2}
${voffset 10}${color3}${freq_g} GHz${alignr}${color3}${loadavg}
#------------+
#CPU CORES
#------------+
${color1}${cpugraph cpu0 20,295}
${color3}${cpu}%${alignr}${color2}${cpubar cpu0 10,225}
${voffset -5}${color1}${cpubar cpu1 5,70}${goto 85}${cpubar cpu2 5,70}${goto 160}${cpubar cpu3 5,70}${goto 235}${cpubar cpu4 5,70}
${voffset -10}${color1}${cpubar cpu5 4,70}${goto 85}${cpubar cpu6 5,70}${goto 160}${cpubar cpu7 5,70}${goto 235}${cpubar cpu8 5,70}
#------------+
# MEMORY
#------------+
${voffset 20}${color1}${font :size=14:bold}MEMORY ${hr 2}${font}
${voffset 10}${color1}RAM${goto 80}${mem}${goto 170}${color3}${membar 10,90}${alignr}${memperc}%
${color1}SWAP${goto 80}${swap}${goto 170}${color3}${swapbar 10,90}${alignr}${swapperc}%
${voffset 10}${color1}Name${goto 200}MEM%${alignr}MEM
${color2}${top_mem name 1} ${goto 180}${top_mem mem 1}${alignr}${top_mem mem_res 1}${color3}
${top_mem name 2} ${goto 180}${top_mem mem 2}${alignr}${top_mem mem_res 2}
${top_mem name 3} ${goto 180}${top_mem mem 3}${alignr}${top_mem mem_res 3}
${top_mem name 4} ${goto 180}${top_mem mem 4}${alignr}${top_mem mem_res 4}
${top_mem name 5} ${goto 180}${top_mem mem 5}${alignr}${top_mem mem_res 5}
#------------+
# PROCESSES
#------------+
${voffset 20}${color1}${font :size=14:bold}PROCESSES ${hr 2}${font}
${voffset 10}${color1}Name${goto 180}CPU%${alignr}MEM%
${color2}${top name 1} ${goto 180}${top cpu 1}${alignr}${top mem 1}${color3}
${top name 2} ${goto 180}${top cpu 2}${alignr}${top mem 2}
${top name 3} ${goto 180}${top cpu 3}${alignr}${top mem 3}
${top name 4} ${goto 180}${top cpu 4}${alignr}${top mem 4}
${top name 5} ${goto 180}${top cpu 5}${alignr}${top mem 5}
#------------+
# DISK
#------------+
${voffset 20}${color1}${font :size=14:bold}DISK ${hr 2}${font}
${voffset 10}${color1}/${goto 80}${fs_free /}${goto 170}${color3}${fs_bar 10,90 /}${alignr}${fs_used_perc /}%
${color1}/home${goto 80}${fs_free /home}${goto 170}${color3}${fs_bar 10,90 /home}${alignr}${fs_used_perc /home}%
#------------+
# NETWORK
#------------+
${voffset 20}${color1}${font :size=14:bold}NETWORK ${hr 2}${font}
${voffset 10}${color1}${wireless_essid wlp0s20f3}${alignr}${wireless_link_qual_perc wlp0s20f3}% ${wireless_link_bar 10,75 wlp0s20f3}
${color3}${upspeedf wlp0s20f3} KiB/s${alignr} ${downspeedf wlp0s20f3} KiB/s
${color1}${upspeedgraph wlp0s20f3 20,130 -l}$alignr${downspeedgraph wlp0s20f3 20, 130 -l}
${color1}Ethernet
${color3}${upspeedf enp0s31f6} KiB/s${alignr} ${downspeedf enp0s31f6} KiB/s
${color1}${upspeedgraph enp0s31f6 20,130 -l}$alignr${downspeedgraph enp0s31f6 20, 130 -l}
]]
